
drop database ecoway;

CREATE DATABASE ecoway;

USE ecoway;

CREATE TABLE fornecedores (
  id INT AUTO_INCREMENT PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  endereco_completo VARCHAR(100) NOT NULL,
  telefone VARCHAR(20) NOT NULL,
  email VARCHAR(100) NOT NULL
);

CREATE TABLE cliente (
  id INT AUTO_INCREMENT PRIMARY KEY,
  senha VARCHAR(30) NOT NULL, 
  nome VARCHAR(50) NOT NULL,
  date_nascimento VARCHAR(30) NOT NULL, 
  endereco VARCHAR(100) NOT NULL,
  telefone VARCHAR(20) NOT NULL,
  email VARCHAR(100) NOT NULL,
  nivel_acesso VARCHAR(100) NOT NULL,
  veiculo_id INT NOT NULL
);

CREATE TABLE reserva (
  id INT AUTO_INCREMENT PRIMARY KEY,
  cliente_id INT NOT NULL,
  veiculo_id INT NOT NULL,
  data_retirada DATE NOT NULL,
  data_devolucao DATE NOT NULL,
  valor_total DECIMAL(10, 2) NOT NULL,
  status_reserva VARCHAR(20) NOT NULL,
  praca_reserva_id INT NOT NULL
);

CREATE TABLE praca (
  id INT AUTO_INCREMENT PRIMARY KEY,
  rua VARCHAR(400) NOT NULL,
  bairro VARCHAR(400) NOT NULL,
  cep VARCHAR(30) NOT NULL,
  telefone_contato VARCHAR(30) NOT NULL,
  email_contato VARCHAR(30) NOT NULL,
  nome_estabelecimento VARCHAR(400) NOT NULL,
  quat_veiculos_disponivel INT NOT NULL,
  qt_prestadores INT NOT NULL,
  veiculo_id INT NOT NULL,
  fornecedor_id INT NOT NULL
);

CREATE TABLE veiculos (
  id INT AUTO_INCREMENT PRIMARY KEY,
  marca VARCHAR(255) NOT NULL,
  modelo VARCHAR(255) NOT NULL,
  cor VARCHAR(255) NOT NULL,
  ano INT NOT NULL,
  placa VARCHAR(255) NOT NULL,
  categoria VARCHAR(255) NOT NULL,
  capacidade_passageiros INT NOT NULL,
  capacidade_carga INT NOT NULL,
  preco_base DECIMAL(10, 2) NOT NULL,
  fornecedor_id INT NOT NULL,
  autonomia VARCHAR(300) NOT NULL,
  tempo_recarga VARCHAR(20) NOT NULL,
  imagem VARCHAR(500)
);

ALTER TABLE reserva ADD CONSTRAINT fk_reserva_cliente 
  FOREIGN KEY (cliente_id) REFERENCES cliente(id);
ALTER TABLE reserva ADD CONSTRAINT fk_reserva_veiculo 
  FOREIGN KEY (veiculo_id) REFERENCES veiculos(id);
ALTER TABLE reserva ADD CONSTRAINT fk_reserva_praca_reserva 
  FOREIGN KEY (praca_reserva_id) REFERENCES praca(id);

ALTER TABLE veiculos ADD CONSTRAINT fk_veiculo_fornecedor 
  FOREIGN KEY (fornecedor_id) REFERENCES fornecedores(id);

ALTER TABLE cliente ADD CONSTRAINT fk_cliente_veiculo 
  FOREIGN KEY (veiculo_id) REFERENCES veiculos(id);

ALTER TABLE praca ADD CONSTRAINT fk_praca_veiculo 
  FOREIGN KEY (veiculo_id) REFERENCES veiculos(id);
ALTER TABLE praca ADD CONSTRAINT fk_praca_fornecedor 
  FOREIGN KEY (fornecedor_id) REFERENCES fornecedores(id);
ALTER TABLE praca ADD CONSTRAINT fk_praca_reserva 
  FOREIGN KEY (id) REFERENCES reserva(praca_reserva_id);



